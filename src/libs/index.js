export const ordinal = n => {
  const s = ['th', 'st', 'nd', 'rd']
  const v = n % 100
  return n + (s[(v - 20) % 10] || s[v] || s[0])
}

export const getMonthyLoan = (monthlyRate, tenure, loanAmount) => {
  const numerator = monthlyRate * (1 + monthlyRate) ** (tenure * 12)
  const denominator = (1 + monthlyRate) ** (tenure * 12) - 1

  return loanAmount * (numerator / denominator)
}

export const getMonthlyRate = (interestRate) => interestRate / 100 / 12.0

export const getTotalRepayment = (tenure, monthyRepayments) => {
  const totalYearlyRepayments = monthyRepayments.reduce((total, n) => total + n * 12, 0)
  const remainingYears = tenure - monthyRepayments.length
  // use last monthly repayment for the remaining tenure years
  const remainingRepayments = monthyRepayments[monthyRepayments.length - 1] * 12 * remainingYears

  return totalYearlyRepayments + remainingRepayments
}
