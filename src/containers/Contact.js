import React from 'react'
import { connect } from 'react-redux'

import { submitContactForm } from '../actions'
import ContactForm from '../components/ContactForm'

const Contact = ({ loading, result, submit }) => {
  return (
    <ContactForm
      loading={loading}
      result={result}
      submit={submit}
    />
  )
}

const mapStateToProps = state => ({
  ...state.contactForm
})

const mapDispatchToProps = dispatch => ({
  submit: data => dispatch(submitContactForm(data))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contact)
