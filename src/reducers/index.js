import { combineReducers } from 'redux'
import { SET_FIELD, REQUEST_RATES, RECEIVE_RATES, SUBMIT_CONTACT_FORM, CHECK_CONTACT_FORM } from '../actions'

const calculatorFields = (
  state = {
    tenure: 20,
    propertyPrice: 450000,
    loanAmount: 0
  },
  action
) => {
  switch (action.type) {
    case SET_FIELD:
      return { ...state, ...action.field }
    default:
      return state
  }
}

const rates = (
  state = {
    loading: false,
    error: null,
    items: []
  },
  action
) => {
  switch (action.type) {
    case REQUEST_RATES:
      return { ...state, loading: true }
    case RECEIVE_RATES:
      return { ...state, loading: false, ...action.results }
    default:
      return state
  }
}

const contactForm = (
  state = {
    loading: false,
    result: null
  },
  action
) => {
  switch (action.type) {
    case SUBMIT_CONTACT_FORM:
      return { ...state, loading: true }
    case CHECK_CONTACT_FORM:
      return { ...state, loading: false, ...action.result }
    default:
      return state
  }
}

export default combineReducers({
  calculatorFields,
  rates,
  contactForm
})
