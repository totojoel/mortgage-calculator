import React, { useState } from 'react'
import styled from 'styled-components'

import {
  ordinal,
  getMonthyLoan,
  getMonthlyRate,
  getTotalRepayment
} from '../libs'
import ProgressBar from './ProgressBar'
import Contact from '../containers/Contact'

const formatAmount = amount =>
  '$' + (+amount.toFixed(2)).toLocaleString('en-us')

const Rate = ({
  bankImageUrl,
  rateName,
  rateTypeName,
  lockInPeriod,
  interestRates,
  interestRatesDetails,
  loanAmount,
  tenure
}) => {
  const [open, setOpen] = useState(false)
  const [showContact, setShowContact] = useState(false)

  const interestRate = interestRates[interestRates.length - 1]
  const monthlyRate = getMonthlyRate(interestRate)
  const monthlyLoan = getMonthyLoan(monthlyRate, tenure, loanAmount)
  const interest = loanAmount * monthlyRate
  const principal = monthlyLoan - interest
  const monthyRepayments = interestRates.map(rate =>
    getMonthyLoan(getMonthlyRate(rate), tenure, loanAmount)
  )
  const totalRepayment = getTotalRepayment(tenure, monthyRepayments)

  return (
    <Wrapper>
      <BasicInfo onClick={() => !open && setOpen(true)}>
        <Image src={bankImageUrl} />
        <BasicInfoGroup>
          <RateName>
            <span>{rateName}</span>
            <br />
            Interest Rate: {interestRate}% &nbsp;{' '}
            <b>{formatAmount(monthlyLoan)}/month</b>
          </RateName>
          <Bar>
            <BarLabel>
              Principal <br />
              <b>{formatAmount(principal)}</b>
            </BarLabel>
            <ProgressBar
              progress={Math.min((interest / principal) * 100, 100)}
            />
            <BarLabel>
              Interest <br />
              <b>{formatAmount(interest)}</b>
            </BarLabel>
          </Bar>
        </BasicInfoGroup>
      </BasicInfo>
      {open && (
        <>
          <TableGroup>
            <div>
              <RateDetail>
                <div>Rate type:</div>
                <div>{rateTypeName}</div>
              </RateDetail>
              <RateDetail>
                <div>Lock-in period:</div>
                <div>{lockInPeriod}</div>
              </RateDetail>
              <RateDetail>
                <div>Total repayment:</div>
                <div>{formatAmount(totalRepayment)}</div>
              </RateDetail>
            </div>
            <InterestTable>
              <tbody>
                <tr>
                  <th>Year</th>
                  <th>Interest Rate</th>
                  <th>Monthly Repayment</th>
                </tr>
                {interestRates.map((interestRate, index) => (
                  <tr key={index}>
                    <td>{ordinal(index + 1)}</td>
                    <td>{interestRate + '% ' + interestRatesDetails[index]}</td>
                    <td>{formatAmount(monthyRepayments[index])}</td>
                  </tr>
                ))}
              </tbody>
            </InterestTable>
          </TableGroup>
          {showContact ? (
            <Contact />
          ) : (
            <ContactSection>
              <div>
                <b>Talk to an Ohymyhome Agent Today!</b>
                <br />
                Request for a non-obligatory, free consultation with us, we call
                you at your preferred timing.
              </div>
              <div>
                <Button onClick={() => setShowContact(true)}>
                  Request Callback
                </Button>
              </div>
            </ContactSection>
          )}
        </>
      )}
    </Wrapper>
  )
}

const ContactSection = styled.div`
  margin-top: 20px;
  div {
    :first-child {
      display: none;
    }
  }
  @media (min-width: 768px) {
    display: grid;
    background: #eeeeee;
    margin-left: -20px;
    margin-right: -20px;
    margin-bottom: -20px;
    padding: 20px;
    grid-gap: 20px;
    grid-template-columns: 290px auto;
    font-size: 12px;
    line-height: 18px;
    align-items: center;

    div {
      margin: auto;
      :first-child {
        display: inline;
      }
    }
  }
`

const BasicInfoGroup = styled.div`
  display: grid;
  align-items: center;
  grid-gap: 10px;
  width: 100%;
  @media (min-width: 768px) {
    grid-template-columns: 200px 330px;
    grid-gap: 20px;
  }
`

const TableGroup = styled.div`
  display: grid;
  flex: 1;
  margin-top: 10px;
  grid-gap: 20px;
  @media (min-width: 768px) {
    grid-template-columns: 290px auto;
  }
`

const BarLabel = styled.span`
  font-size: 10px;
  text-align: center;
  :first-child {
    margin-right: 10px;
  }
  :last-child {
    margin-left: 10px;
  }
`

const Bar = styled.div`
  display: flex;
  align-items: center;
`

const Button = styled.button`
  border: unset;
  font-family: unset;
  font-size: 14px;
  background: #f25d00;
  color: white;
  border-radius: 4px;
  text-align: center;
  padding: 4px 20px;
  cursor: pointer;
  @media (max-width: 768px) {
    padding: 4px;
    width: 100%;
  }
  padding-top: 5px;
`

const InterestTable = styled.table`
  border-collapse: collapse;
  font-size: 12px;
  td,
  th {
    padding: 10px;
    text-align: center;
  }

  tr:nth-child(even) {
    background: #eeeeee;
  }
  td:nth-child(2) {
    @media (min-width: 319px) {
      white-space: nowrap;
    }
  }
  th {
    color: white;
    background: #bdbdbd;
  }
`

const RateDetail = styled.div`
  display: flex;
  font-size: 12px;
  line-height: 18px;
  div {
    flex: 1;
    :first-child {
      margin-right: 10px;
      font-weight: bold;
    }
  }
`

const RateName = styled.div`
  font-size: 12px;
  > span {
    font-size: 14px;
    font-weight: bold;
  }
`

const Image = styled.img`
  height: 80px;
  margin-right: 10px;
`

const BasicInfo = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
`

const Wrapper = styled.div`
  box-shadow: 0 0 4px #bdbdbd;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
  padding: 20px;
  margin-bottom: 20px;
  min-width: 370px;
  @media (max-width: 450px) {
    min-width: unset;
    width: calc(100% - 20px);
    padding: 10px;
  }
`

export default Rate
