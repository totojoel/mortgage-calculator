import React from 'react'
import styled from 'styled-components'

import Rate from './Rate'

const RatesList = ({ rates, loanAmount, tenure }) => (
  <Wrapper>
    <Scroller>
      {rates.loading ? (
        <div>loading</div>
      ) : (
        rates.items.map(rate => (
          <Rate
            key={rate._id}
            loanAmount={loanAmount}
            tenure={tenure}
            {...rate}
          />
        ))
      )}
    </Scroller>
  </Wrapper>
)

const Scroller = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  padding: 20px;
`

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  background: white;
  position: relative;
  overflow: auto;
  height: 100%;
  width: 100%;
`

export default RatesList
