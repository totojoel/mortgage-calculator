import React, { useState } from 'react'
import styled from 'styled-components'

const ContactForm = ({ loading, result, submit }) => {
  const [values, setValues] = useState({
    fullname: '',
    email: '',
    mobile: ''
  })
  const { fullname, email, mobile } = values

  return (
    <Wrapper>
      <div>Please input your contact details</div>
      {loading ? (
        <div>loading</div>
      ) : (
        <Group>
          <Field
            value={fullname || ''}
            name='fullname'
            placeholder='Fullname'
            onChange={e => setValues({ ...values, fullname: e.target.value })}
          />
          <Field
            value={email || ''}
            name='email'
            placeholder='Email'
            onChange={e => setValues({ ...values, email: e.target.value })}
          />
          <Field
            value={mobile || ''}
            name='mobile'
            placeholder='Mobile'
            onChange={e => setValues({ ...values, mobile: e.target.value })}
          />
          <Button onClick={() => submit(values)}>Submit</Button>
        </Group>
      )}
    </Wrapper>
  )
}

const Button = styled.button`
  border: unset;
  font-family: unset;
  font-size: 14px;
  background: #f25d00;
  color: white;
  border-radius: 4px;
  text-align: center;
  padding: 4px 20px;
  cursor: pointer;
  padding-top: 5px;
`

const Group = styled.div`
  display: grid;
  grid-gap: 10px;
  @media (min-width: 450px) {
    grid-template-columns: auto auto auto auto;
  }
`

const Field = styled.input`
  all: unset;
  box-sizing: border-box;
  width: 100%;
  padding: 4px;
  padding-top: 3px;
  border: solid 1px #bdbdbd;
  text-align: center;
  font-size: 12px;
  border-radius: 4px;
`

const Wrapper = styled.div`
  background: #eeeeee;
  margin: -20px;
  margin-top: 20px;
  padding: 20px;
  @media (max-width: 450px) {
    margin: -10px;
    margin-top: 20px;
    padding: 10px;
  }
  div:first-child {
    font-size: 14px;
    font-weight: bold;
    margin-bottom: 10px;
  }
`

export default ContactForm
