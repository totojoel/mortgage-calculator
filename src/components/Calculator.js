import React from 'react'
import styled from 'styled-components'
import NumberFormat from 'react-number-format'

import RatesList from './RatesList'

const Calculator = ({
  tenureOptions,
  tenure,
  propertyPrice,
  loanAmount,
  setField,
  rates
}) => (
  <BackgroundOverlay>
    <Wrapper>
      <Header>
        <HeaderIcon alt='icon' />
        <Title>
          <strong>Monthly Payment Calculator</strong>
          <br />
          Increasing the prosperity in our lives can be accomplished by having the right grame of mind.
        </Title>
      </Header>
      <Body>
        <Group>
          <Field
            label='Property Price'
            value={propertyPrice}
            onChange={value => setField({ propertyPrice: value })}
          />
          <Field
            label='Loan Amount'
            value={loanAmount}
            onChange={value => setField({ loanAmount: value })}
          />
        </Group>
        <FieldWrapper>
          <Label>Tenure:</Label>
          <Options>
            {tenureOptions.map((option, index) => (
              <TenureOption
                key={index}
                active={option === tenure}
                onClick={() => setField({ tenure: option })}
              >
                {option}
              </TenureOption>
            ))}
          </Options>
        </FieldWrapper>
        <Button>Recalculate</Button>
      </Body>
    </Wrapper>
    <RatesList rates={rates} loanAmount={loanAmount} tenure={tenure} />
  </BackgroundOverlay>
)

const Field = ({ label, value, onChange }) => (
  <FieldWrapper>
    <Label>{label}:</Label>
    <NumberFormat
      value={value}
      onValueChange={({ value }) => onChange(value)}
      customInput={StyledInput}
      thousandSeparator
      prefix='$'
    />
  </FieldWrapper>
)

const Options = styled.div`
  display: flex;
  justify-content: space-around;
`

const TenureOption = styled.div`
  flex: 1;
  cursor: pointer;
  padding: 4px;
  padding-top: 5px;
  text-align: center;
  font-size: 12px;
  border: solid 1px #bdbdbd;
  color: #bdbdbd;
  border-radius: 4px;
  :not(:last-child) {
    margin-right: 10px;
  }
  ${({ active }) =>
    active &&
    `
    background: #f25d00;
    border-color: #f25d00;
    color: white;
  `}
`

const StyledInput = styled.input`
  all: unset;
  box-sizing: border-box;
  width: 100%;
  padding: 4px;
  padding-top: 3px;
  border: solid 1px #bdbdbd;
  text-align: center;
  font-size: 16px;
  font-weight: bold;
  border-radius: 4px;
`

const FieldWrapper = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
`

const Label = styled.label`
  font-size: 14px;
  margin-bottom: 4px;
  white-space: nowrap;
`

const Button = styled.button`
  font-family: unset;
  background: none;
  font-size: 14px;
  color: #f25d00;
  border: solid 1px;
  border-radius: 4px;
  padding: 4px;
  padding-bottom: 3px;
  text-align: center;
  cursor: pointer;
`

const Group = styled.div`
  display: flex;
  font-size: 14px;
  div:first-child {
    margin-right: 10px;
  }
`

const Body = styled.div`
  display: grid;
  grid-gap: 20px;
  background: white;
  justify-items: stretch;
  align-items: flex-end;
  padding: 20px;
  border-radius: 4px;
  border-top-right-radius: 0px;
  border-top-left-radius: 0px;
  @media (min-width: 667px) {
    grid-template-columns: auto 30% 20%;
    grid-gap: 30px;
  };
`

const Title = styled.div`
  font-size: 12px;
  line-height: 18px;
`

const HeaderIcon = styled.img`
  background: blue;
  margin-right: 10px;
  flex: none;
`

const Header = styled.div`
  background: #eeeeee;
  display: flex;
  align-items: center;
  padding: 10px 20px;
  border-radius: 4px;
  border-bottom-right-radius: 0px;
  border-bottom-left-radius: 0px;
`

const Wrapper = styled.div`
  box-shadow: 0 0 4px #616161;
  border-radius: 4px;
  max-width: 90%;
  margin-left: auto;
  margin-right: auto;
  margin-top: 60px;
  margin-bottom: 20px;
`

const BackgroundOverlay = styled.div`
  background: rgba(0, 0, 0, 0.5);
  display: flex;
  flex-direction: column;
  position: absolute;
  height: 100%;
  width: 100%;
`

export default Calculator
