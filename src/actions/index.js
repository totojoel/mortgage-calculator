import fetch from 'cross-fetch'

export const SET_FIELD = 'SET_FIELD'
export const setField = field => ({
  type: SET_FIELD,
  field
})

export const REQUEST_RATES = 'REQUEST_RATES'
export const requestRates = () => {
  return {
    type: REQUEST_RATES
  }
}

export const RECEIVE_RATES = 'RECEIVE_RATES'
export const receiveRates = results => {
  return {
    type: RECEIVE_RATES,
    results
  }
}

export const SUBMIT_CONTACT_FORM = 'SUBMIT_CONTACT_FORM'
export const submitContactForm = (data) => {
  fetch('/techtest/calculator/form/submit', {
    method: 'POST',
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return {
    type: SUBMIT_CONTACT_FORM
  }
}

export const CHECK_CONTACT_FORM = 'CHECK_CONTACT_FORM'
export const checkContactForm = (result) => {
  return {
    type: CHECK_CONTACT_FORM,
    result
  }
}

export const fetchRates = () => dispatch => {
  dispatch(requestRates())
  return fetch('/techtest/calculator/bankRates')
    .then(res => res.json())
    .then(({ results, error }) => {
      if (error) {
        dispatch(receiveRates({ error: error }))
      }
      if (results) {
        dispatch(receiveRates({ items: [...results] }))
      }
    })
}
