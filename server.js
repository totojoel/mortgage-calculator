const express = require('express')
const axios = require('axios')
const bodyParser = require('body-parser')
require('dotenv').config()

const app = express()
axios.defaults.headers.common['OmhWeb-Auth'] = process.env.TOKEN

app.use(bodyParser.json())

app.get('/techtest/calculator/bankRates', (req, res) => {
  axios
    .get('https://cms.staging.ohmyhome.io/techtest/calculator/bankRates')
    .then(response => res.send(response.data))
    .catch(error => res.send({ error: error.message }))
})

app.get('/techtest/calculator/bankRates/simplified', (req, res) => {
  axios
    .get(
      'https://cms.staging.ohmyhoapp.use(bodyParser.json())me.io/techtest/calculator/bankRates/simplified'
    )
    .then(response => res.send(response.data))
    .catch(error => res.send({ error: error.message }))
})

app.get('/techtest/calculator/bankRate/info', (req, res) => {
  const bankRateId = req.query.bankRate_id
  axios
    .get(
      'https://cms.staging.ohmyhome.io/techtest/calculator/bankRate/info?bankRate_id=' +
        bankRateId
    )
    .then(response => res.send(response.data))
    .catch(error => res.send({ error: error.message }))
})

app.post('/techtest/calculator/form/submit', (req, res) => {
  // unfinished, undocumented form requirements
  axios
    .post('https://cms.staging.ohmyhome.io/techtest/calculator/form/submit', {
      data: req.body
    })
    .then(response => console.log(response.data))
    .catch(error => console.log({ error: error.message }))
})

const PORT = process.env.PORT || 8000
app.listen(PORT, () => console.log(`listening on ${PORT}`))
